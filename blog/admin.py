from django.contrib import admin

from .models import Post, Category


# Register your models here.
@admin.register(Post)
class AdminPost(admin.ModelAdmin):
    list_display = ('title', 'author_name', 'category', 'is_published', 'is_special', 'published_date')
    list_display_links = ('title', 'author_name', 'category', 'published_date')
    search_fields = ('title', 'author_name',)
    list_filter = ('is_special', 'published_date', 'category')

    editable = True
    list_editable = ['is_published', 'is_special']



@admin.register(Category)
class AdminCategory(admin.ModelAdmin):
    list_display = ('title', 'priority')
    editable = True
    list_editable = ['priority', ]
