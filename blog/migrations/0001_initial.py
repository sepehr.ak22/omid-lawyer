# Generated by Django 3.1.1 on 2020-10-07 11:06

import ckeditor.fields
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import painless.utils.upload.path


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, unique=True, validators=[django.core.validators.MaxLengthValidator(150), django.core.validators.MinLengthValidator(3)], verbose_name='عنوان')),
                ('slug', models.SlugField(allow_unicode=True, editable=False, max_length=150, unique=True, verbose_name='نامک')),
                ('priority', models.IntegerField(unique=True, verbose_name='ترتیب')),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
                'ordering': ['priority'],
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, unique=True, validators=[django.core.validators.MaxLengthValidator(150), django.core.validators.MinLengthValidator(3)], verbose_name='عنوان')),
                ('slug', models.SlugField(allow_unicode=True, editable=False, max_length=150, unique=True, verbose_name='نامک')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='بروزرسانی در')),
                ('picture', models.ImageField(height_field='height_field', max_length=110, upload_to=painless.utils.upload.path.date_directory_path, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])], verbose_name='تصویر', width_field='width_field')),
                ('alternate_text', models.CharField(max_length=110, validators=[django.core.validators.MaxLengthValidator(150), django.core.validators.MinLengthValidator(3)], verbose_name='توضیحات تصویر')),
                ('width_field', models.PositiveSmallIntegerField(editable=False, verbose_name='طول تصویر')),
                ('height_field', models.PositiveSmallIntegerField(editable=False, verbose_name='عرض تصویر')),
                ('Summary', models.TextField(max_length=170, verbose_name='توضیحات مختصر')),
                ('is_special', models.BooleanField(default=True, verbose_name='ویژه')),
                ('author_name', models.CharField(max_length=30, verbose_name='نویسنده')),
                ('description', ckeditor.fields.RichTextField(verbose_name='توضیحات کامل')),
                ('reference', models.CharField(blank=True, max_length=80, null=True, verbose_name='ذکر منبع')),
                ('reference_link', models.URLField(blank=True, max_length=80, null=True, verbose_name='آدرس منبع')),
                ('keywords_tag', models.CharField(blank=True, max_length=200, null=True, verbose_name='کلمات کلیدی')),
                ('published_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='زمان انتشار')),
                ('is_published', models.CharField(choices=[('P', 'منتشر شده'), ('D', 'پیش نویس')], max_length=1, verbose_name='وضعیت')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='posts', to='blog.category', verbose_name='دسته\u200cبندی')),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
                'ordering': ['-published_date'],
            },
        ),
    ]
