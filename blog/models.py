from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify

from painless.utils.models.common import TitleSlugLinkModelMixin, TimeStampModelMixin, ImageModelMixin


# Create your models here.
class Post(TitleSlugLinkModelMixin, TimeStampModelMixin, ImageModelMixin):
    CHOICES = [
        ('P', 'منتشر شده'),
        ('D', 'پیش نویس'),
    ]
    Summary = models.TextField(max_length=170, verbose_name='توضیحات مختصر')
    is_special = models.BooleanField(default=True, verbose_name='ویژه')
    author_name = models.CharField(max_length=30, verbose_name='نویسنده')
    category = models.ForeignKey('Category', on_delete=models.SET_NULL,
                                 related_name='posts', verbose_name='دسته‌بندی',
                                 null=True, blank=True)
    description = RichTextField(verbose_name='توضیحات کامل')
    reference = models.CharField(max_length=80, null=True, blank=True, verbose_name='ذکر منبع')
    reference_link = models.URLField(max_length=80, null=True, blank=True, verbose_name='آدرس منبع')
    keywords_tag = models.CharField(max_length=200, null=True, blank=True, verbose_name='کلمات کلیدی')
    published_date = models.DateTimeField(default=timezone.now, verbose_name='زمان انتشار')
    is_published = models.CharField(max_length=1, choices=CHOICES, verbose_name='وضعیت')
    created = models.DateTimeField(auto_now_add=timezone.now)
    updated = models.DateTimeField(auto_now=timezone.now)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        self.validate_unique()
        super(Post, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'
        ordering = ['-published_date']

    def get_absolute_url(self):
        return reverse('page:detail', kwargs={'slug': self.slug})



class Category(TitleSlugLinkModelMixin):
    priority = models.IntegerField(unique=True, verbose_name='ترتیب')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        self.validate_unique()
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['priority', ]

    def get_absolute_url(self):
        return reverse('page:category', kwargs={'slug': self.slug, 'page':1})
