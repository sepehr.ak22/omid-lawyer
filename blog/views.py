from django.shortcuts import get_object_or_404

from django.shortcuts import render

from django.views.generic import (
    ListView,
    DetailView,
    View
)
from django.utils.translation import ugettext_lazy as _

from django.core.paginator import (
    Paginator,
    EmptyPage,
    PageNotAnInteger
)

from blog.models import Post, Category

from cms.models import Cms, Quote, Criminal


class PostListView(ListView):
    page_name = _('مقالات')
    paginate_by = 4
    queryset = Post.objects.filter(is_published='P')
    template_name = 'pages/docs.html'
    context_object_name = 'posts_list'

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(PostListView, self).get_context_data()

        context['site'] = cms
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context


class CategoryListView(ListView):
    page_name = _('مقالات')
    queryset = Category.objects.all()
    template_name = 'pages/docs.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site'] = Cms.objects.first()
        return context

    # def get_template_names(self):
    #     pass


class PostDetailView(DetailView):
    page_name = _('مقالات')
    queryset = Post.objects.filter(is_published='P')
    template_name = 'pages/specific_doc.html'
    context_object_name = 'post_object'


    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(PostDetailView, self).get_context_data()

        context['site'] = cms
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context


class ListCategoryPosts(View):

    template_name = 'pages/category-posts.html'

    paginated_by = 4

    page_name = _('مقالات')


    def get_context_data(self, **kwargs):

        cms = Cms.objects.first()
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = dict()

        context['site'] = cms
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context


    def get(self, request, slug, page, *args, **kwargs):

        category = get_object_or_404(Category, slug=slug)

        posts = category.posts.filter(is_published='P')

        paginator = Paginator(posts, self.paginated_by)

        try:

            posts = paginator.get_page(page)

        except PageNotAnInteger:

            posts = paginator.get_page(1)

        except EmptyPage:

            posts = paginator.page(paginator.num_pages)

        context = self.get_context_data()

        context['category_posts'] = posts

        context['category'] = category

        return render(
            request,
            self.template_name,
            context
        )