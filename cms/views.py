# Create your views here.
from django.views.generic import ListView, DetailView
from django.utils.translation import ugettext_lazy as _

from blog.models import Post, Category
from cms.models import Cms, Quote, Criminal
from .models import Role, Sub_Role


class RoleListView(ListView):
    queryset = Role.objects.all()
    template_name = 'pages/laws.html'
    context_object_name = 'roles'

    page_name = _('قوانین')

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()
        sub_roles = Sub_Role.objects.all()

        context = super(RoleListView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal
        context['sub_roles'] = sub_roles

        return context


class CriminalListView(ListView):
    queryset = Criminal.objects.all()
    # slug_field = 'slug'
    template_name = 'pages/criminal.html'
    context_object_name = 'criminals_list'

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()
        sub_roles = Sub_Role.objects.all()

        context = super(CriminalListView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal
        context['sub_roles'] = sub_roles

        return context
