import secrets

from django.db import models
from django.urls import reverse
from django.utils.text import slugify

from django.utils.translation import ugettext_lazy as _
from painless.utils.upload.path import date_directory_path
from painless.utils.models.common import TitleSlugLinkModelMixin, SVGModelMixin

from django.core.validators import (
    FileExtensionValidator,
    MinLengthValidator,
    MaxLengthValidator
)


class Criminal(TitleSlugLinkModelMixin, SVGModelMixin):
    tab = models.CharField(
        _("تب"),
        max_length=10,
        unique=True,
        editable=False
    )

    svg_dark = models.FileField(
        _("فایل مشکی (svg)"),
        upload_to=date_directory_path,
        max_length=110,
        help_text='فرمت فایل باید svg باشد',
        validators=[FileExtensionValidator(allowed_extensions=['svg', 'SVG', ])]
    )

    svg_dark_alt_text = models.CharField(
        _("توضیحات ایکن مشکی"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )

    description = models.TextField(verbose_name='توضیحات')
    priority = models.IntegerField(unique=True, verbose_name='ترتیب')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.tab = secrets.token_urlsafe(3).lower()
        self.slug = secrets.token_urlsafe(3).lower()
        self.validate_unique()
        super(Criminal, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Criminal'
        verbose_name_plural = 'Criminals'
        ordering = ['priority']

    def get_absolute_url(self):
        return reverse('page:criminal')
        # return reverse('page:criminal', kwargs={'slug': '#'+self.tab})
