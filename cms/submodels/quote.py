from django.db import models


class Quote(models.Model):
    title = models.CharField(max_length=100, verbose_name='عنوان')
    author = models.CharField(max_length=30, verbose_name="نویسنده")

    def __str__(self):
        return self.title

    def random_choices(self):
        pass

    class Meta:
        verbose_name = 'quote'
        verbose_name_plural = 'quotes'

    def get_absolute_url(self):
        pass
