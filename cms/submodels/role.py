from django.db import models


class Role(models.Model):
    title = models.CharField(max_length=80, verbose_name='عنوان')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Rule Category'
        verbose_name_plural = 'Rules Category'

    def get_absolute_url(self):
        pass


class Sub_Role(models.Model):
    title = models.CharField(max_length=30, verbose_name='عنوان')
    link = models.CharField(max_length=200, verbose_name='لینک')
    role = models.ForeignKey('Role',
                             on_delete=models.CASCADE,
                             related_name='roles',
                             verbose_name='دسته‌بندی'
                             )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Rule'
        verbose_name_plural = 'Rules'

    def get_absolute_url(self):
        pass
