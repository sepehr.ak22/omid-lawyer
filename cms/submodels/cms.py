from django.core.validators import (
    FileExtensionValidator,
    MinLengthValidator,
    MaxLengthValidator
)
from django.db import models
from django.utils.translation import ugettext_lazy as _

from painless.utils.models.common import ImageModelMixin
from painless.utils.upload.path import date_directory_path


class Cms(ImageModelMixin):
    full_name = models.CharField(max_length=30, verbose_name='نام و نام خانوادگی')
    job = models.CharField(max_length=100, verbose_name='شغل')
    phone = models.CharField(max_length=13, verbose_name='همراه')
    email = models.CharField(max_length=30, verbose_name='ایمیل')
    footer_txt = models.TextField(verbose_name='متن پایین صفحه')
    instagram = models.URLField(
        _("اینستاگرام"), 
        max_length = 200,
        blank=True,
        null=True
    )
    telegram = models.URLField(
        _("تلگرام"), 
        max_length = 200,
        blank=True,
        null=True
    )
    about_header = models.CharField(max_length=100, verbose_name='عنوان درباره‌ما')
    about_text = models.TextField(verbose_name='توضیحات درباره‌ما')

    picture_about = models.ImageField(
        _("تصویر درباره ما"),
        upload_to=date_directory_path,
        height_field='width_field_about',
        width_field='height_field_about',
        max_length=110,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    alternate_text_about = models.CharField(
        _("توضیحات تصویر درباره ما"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )
    width_field_about = models.PositiveSmallIntegerField(_("طول تصویر"), editable=False)
    height_field_about = models.PositiveSmallIntegerField(_("عرض تصویر"), editable=False)

    picture_footer = models.ImageField(
        _("تصویر پایین صفحه"),
        upload_to=date_directory_path,
        height_field='width_field_footer',
        width_field='height_field_footer',
        max_length=110,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    alternate_text_footer = models.CharField(
        _("توضیحات تصویر پایین صفحه"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )
    width_field_footer = models.PositiveSmallIntegerField(_("طول تصویر"), editable=False)
    height_field_footer = models.PositiveSmallIntegerField(_("عرض تصویر"), editable=False)

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = 'Setting'
        verbose_name_plural = 'Settings'

    def get_absolute_url(self):
        pass
