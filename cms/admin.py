from django.contrib import admin

from .models import Criminal, Cms, Quote, Role, Sub_Role


@admin.register(Cms)
class CmsAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'job', 'phone',)
    list_display_links = ('full_name', 'job', 'phone',)

    fields = (
        ('full_name', 'job', 'picture', 'alternate_text',),
        ('phone', 'email',),
        ('instagram', 'telegram',),
        (
            'about_header', 'about_text',
            'picture_about', 'alternate_text_about',
            'picture_footer', 'alternate_text_footer',
        ),
        'footer_txt',
    )


@admin.register(Criminal)
class CriminalAdmin(admin.ModelAdmin):
    list_display = ('title', 'priority','tab')
    search_fields = ('title',)
    editable = True
    list_editable = ['priority', ]


@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    list_display = ('title', 'author',)
    list_display_links = ('title', 'author',)
    search_fields = ('title', 'author',)


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(Sub_Role)
class Sub_roleAdmin(admin.ModelAdmin):
    list_display = ('title', 'role',)
    list_display_links = ('title', 'role',)
    list_filter = ('role',)
    search_fields = ('title',)

    fields = (
        ('title',),
        ('link',),
        ('role',),
    )
