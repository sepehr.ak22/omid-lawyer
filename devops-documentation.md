

<!-- Database Creation Help -->

CREATE USER <user_name> WITH PASSWORD '<password>';
CREATE DATABASE <db_name>;
ALTER ROLE <user_name> SET client_encoding TO 'utf8';
ALTER ROLE <user_name> SET default_transaction_isolation TO 'read committed';
ALTER ROLE <user_name> SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE <db_name> TO <user_name>;

<!-- Django Admin Interface -->
python manage.py loaddata admin_interface_theme_uswds.json
>>> leltajmil\venv\Lib\site-packages\django\contrib\admin\templates\admin\base_site.html
{% extends "admin_interface:admin/base_site.html" %}

<!-- Django Migrations -->
python manage.py makemigrations
python manage.py migrate

<!-- Django Deployment Check -->
python manage.py collectstatic
python manage.py check
python manage.py test
python manage.py check --deploy

<!-- Multi Language Command -->

***** Dont use these commands until development team tell devops engineer.
<!-- python manage.py makemessages --ignore="static" --ignore="venv" -l ar -->
<!-- python manage.py compilemessages -->

<!-- package management -->
## if rcssmin or jsmin in django compressor doesnt install
pip install rcssmin --install-option="--without-c-extensions"
pip install rjsmin --install-option="--without-c-extensions"
pip install django-compressor --upgrade

## if psycopg2 doesnt install
pip install psycopg2-binary


<!-- get project from git -->
git clone https://gitlab.com/sepehr.ak22/omid-lawyer.git