# Create your views here.
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _

from blog.models import Post, Category
from cms.models import Cms, Quote, Criminal
from .models import Message


class CreateContactUsView(SuccessMessageMixin, CreateView):
    model = Message

    page_name = _('تماس با ما')

    fields = [
        'fname', 'lname', 'phone', 'email', 'ordertime', 'message', 'subject'
    ]

    template_name = 'pages/contact-us.html'

    success_url = reverse_lazy('page:contact_us')

    success_message = _("پیام شما ثبت شد. سریعا با شما تماس خواهیم گرفت")

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(CreateContactUsView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context
