# Generated by Django 3.1.1 on 2020-10-07 11:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0002_auto_20200930_1429'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='ordertime',
            field=models.CharField(choices=[('E', 'دریافت پاسخ از طریق ایمیل'), ('S', 'دریافت پاسخ از طریق پیامک')], default='E', max_length=1, verbose_name='ارسال پاسخ از طریق'),
        ),
    ]
