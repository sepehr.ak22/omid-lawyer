from django.contrib import admin
from .models import Message

# Register your models here.
@admin.register(Message)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('fname', 'lname', 'subject', 'phone', 'email', 'ordertime')
    list_filter = ('ordertime', )
    search_fields = ('fname', 'lname', 'subject')
    list_display_links = ('fname', 'lname', 'subject', 'phone', 'email', 'ordertime')

    fields = (
        ('fname', 'lname', ),
        ('subject', 'ordertime', ),
        ('phone', 'email', ),
        ('message', ),
    )
