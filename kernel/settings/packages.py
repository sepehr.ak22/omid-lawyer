from .base import INSTALLED_APPS

# ############## #
#   EXTENSIONS   #
# ############## #
INSTALLED_APPS.append('painless',)
INSTALLED_APPS.append('ckeditor',)
INSTALLED_APPS.append('djrichtextfield',)
INSTALLED_APPS.append('admin_interface', )
INSTALLED_APPS.append('admin_honeypot', )
INSTALLED_APPS.append('colorfield', )
INSTALLED_APPS.append('widget_tweaks', )

# ############## #
# CUSTOM PROJECT #
# ############## #
INSTALLED_APPS.append('cms',)
INSTALLED_APPS.append('pages',)
INSTALLED_APPS.append('blog',)
INSTALLED_APPS.append('contact',)
INSTALLED_APPS.append('advises',)
INSTALLED_APPS.append('arbitrator',)

# ############################# #
# CONFIG DJANGO ADMIN INTERFACE #
# ############################# #
X_FRAME_OPTIONS = 'SAMEORIGIN'

# ######################### #
#         CKEDITOR          #
# ######################### #

CKEDITOR_UPLOAD_PATH = "ckeditor/"

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Styles', 'Format', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike', 'SpellChecker', 'Undo', 'Redo'],
            ['Image', 'Table', 'HorizontalRule'],
            ['TextColor', 'BGColor'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['NumberedList', 'BulletedList'],
            ['Indent', 'Outdent'],
            ['Maximize', ],

        ],
        'extraPlugins': 'justify,liststyle,indent',
    },
}
