from django.contrib import admin
from .models import Arbiter

# Register your models here.
@admin.register(Arbiter)
class ArbiterAdmin(admin.ModelAdmin):
    list_display = ('fname', 'lname', 'phone', 'email', 'ordertime')
    list_display_links = ('fname', 'lname', 'phone', 'email', 'ordertime')
    search_fields = ('fname', 'lname')
    list_filter = ('ordertime',)

