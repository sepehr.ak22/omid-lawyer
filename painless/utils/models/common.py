from django.core.validators import (
    FileExtensionValidator,
    MinLengthValidator,
    MaxLengthValidator
)
from django.db import models
from django.utils.translation import ugettext_lazy as _

from painless.utils.upload.path import date_directory_path


class TitleSlugLinkModelMixin(models.Model):
    title = models.CharField(
        _("عنوان"),
        max_length=150,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ],
        unique=True
    )
    slug = models.SlugField(
        _("نامک"),
        editable=False,
        allow_unicode=True,
        max_length=150,
        unique=True
    )

    class Meta:
        abstract = True


class TimeStampModelMixin(models.Model):
    created = models.DateTimeField(_("ایجاد شده در"), auto_now_add=True)
    modified = models.DateTimeField(_("بروزرسانی در"), auto_now=True)

    class Meta:
        abstract = True


class ImageModelMixin(models.Model):
    picture = models.ImageField(
        _("تصویر"),
        upload_to=date_directory_path,
        height_field='height_field',
        width_field='width_field',
        max_length=110,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    alternate_text = models.CharField(
        _("توضیحات تصویر"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )
    width_field = models.PositiveSmallIntegerField(_("طول تصویر"), editable=False)
    height_field = models.PositiveSmallIntegerField(_("عرض تصویر"), editable=False)

    class Meta:
        abstract = True


class SVGModelMixin(models.Model):
    svg = models.FileField(
        _("فایل (svg)"),
        upload_to=date_directory_path,
        max_length=110,
        help_text='فرمت فایل باید svg باشد',
        validators=[FileExtensionValidator(allowed_extensions=['svg', 'SVG', ])]
    )

    svg_alt_text = models.CharField(
        _("توضیحات ایکن"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )

    class Meta:
        abstract = True
