# Create your views here.
from django.views.generic import CreateView
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin

from blog.models import Post, Category
from cms.models import Cms, Quote, Criminal
from .models import Consulting
from .forms import AdviseFrom


class CreateAdvisesView(SuccessMessageMixin, AdviseFrom, CreateView):
    model = Consulting

    # form = AdviseFrom

    page_name = _('دریافت مشاوره')

    template_name = 'pages/get-advises.html'

    success_url = reverse_lazy('page:advises')

    success_message = _("به زودی برای هماهنگی مشاوره با شما تماس خواهیم گرفت.")


    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(CreateAdvisesView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        return super().post(request, *args, **kwargs)