from django.core.validators import (
    EmailValidator,
    RegexValidator,
    MaxLengthValidator,
    MinLengthValidator
)
from django.db import models
# Create your models here.
from django.urls import reverse

from painless.utils.regex.patterns import PERSIAN_PHONE_NUMBER_PATTERN


class Consulting(models.Model):
    CHOICES = [
        ('E', 'دریافت پاسخ از طریق ایمیل'),
        ('S', 'دریافت پاسخ از طریق پیامک'),
    ]

    fname = models.CharField(
        max_length=30,
        validators=[
            MaxLengthValidator(30, message='حداکثر تا ۳۰ کاراکتر وارد کنید'),
            MinLengthValidator(3, message='شما نمیتوانید کمتر از ۳ کاراتر وارد کنید')
        ],
        verbose_name='نام'
    )

    lname = models.CharField(
        max_length=30,
        validators=[
            MaxLengthValidator(30, message='حداکثر تا ۳۰ کاراکتر وارد کنید'),
            MinLengthValidator(3, message='شما نمیتوانید کمتر از ۳ کاراتر وارد کنید')
        ],
        verbose_name='نام خانوادگی'
    )

    phone = models.CharField(
        max_length=13,
        validators=[
            MaxLengthValidator(13, message='شماره تلفن نباید بیشتر از ۱۳ رقم باشد'),
            MinLengthValidator(11, message='شماره تلفن باید ۱۱ رقم باشد'),
            RegexValidator(PERSIAN_PHONE_NUMBER_PATTERN,
                           message='شماره تماس را به صورت صحیح وارد نمایید. (۰۹۸۸۴۴۴۳۳۳۳)')
        ],
        verbose_name='تلفن همراه'
    )

    email = models.EmailField(
        validators=[
            EmailValidator(message='لطفا ایمیل را به درستی وارد نمایید.')
        ],
        verbose_name='ایمیل'
    )

    ordertime = models.CharField(
        max_length=1,
        choices=CHOICES,
        default='E',
        verbose_name='ارسال پاسخ از طریق'
    )

    message = models.TextField(
        max_length=500,
        validators=[
            MaxLengthValidator(500, message='پیام نمیتواند بیش از ۵۰۰ کاراکتر باشد'),
        ],
        verbose_name='توضیحات'
    )

    def __str__(self):
        return "{} {}".format(self.fname, self.lname)

    class Meta:
        verbose_name = 'advise'
        verbose_name_plural = 'advises'

    def get_absolute_url(self):
        return reverse('page:advises')
