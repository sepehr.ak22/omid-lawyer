from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from advises.models import Consulting


class AdviseFrom(forms.ModelForm):
    class Meta:
        model = Consulting
        fields = [
            'fname', 'lname', 'phone', 'email', 'ordertime', 'message',
        ]

    def clean_fname(self):
        fname = self.cleaned_data.get('fname')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in fname:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return fname

    def clean_lname(self):
        lname = self.cleaned_data.get('lname')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in lname:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return lname

    def clean_message(self):
        message = self.cleaned_data.get('message')

        except_values = ['<', '>', '/', '+']

        for e_v in except_values:

            if e_v in message:
                raise ValidationError(
                    _('شما نمی توانید کاراکتر های خاص در این فیلد استفاده کنید.'),
                    code='forbidden'
                )

        return message
