# CONTRIBUTORS:

| Name                          | Role                     |
| ----------------------------- |:------------------------:|
| Sepehr Akbarzadeh             | Project Manager          |
| Banafsheh Soleimanpoorian     | UI & UX Designer         |
| Mehrdad Mirzaei               | DevOps Engineer          |
| Hassan Mohamadian             | Back-End Developer       |
| Alisina Saemi                 | Front-End Developer      |
