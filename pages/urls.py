from django.urls import re_path
from .views import HomeTemplateView, AboutUsTemplateView, SearchResultsView
from blog.views import PostListView, PostDetailView, ListCategoryPosts
from advises.views import CreateAdvisesView
from arbitrator.views import CreateArbiterView
from contact.views import CreateContactUsView
from cms.views import RoleListView, CriminalListView

app_name = 'page'

urlpatterns = [
    re_path(r'^$', HomeTemplateView.as_view(), name='index'),
    re_path(r'^about_us$', AboutUsTemplateView.as_view(), name='about_us'),
    re_path(r'^posts/$', PostListView.as_view(), name='posts'),
    re_path(r'^posts/page/(?P<page>[\d]+)/$', PostListView.as_view(), name='posts'),
    re_path(r'^search/$', SearchResultsView.as_view(), name='search'),
    re_path(r'^search/page/(?P<page>[\d]+)/$', SearchResultsView.as_view(), name='search'),
    re_path(r'^advises/$', CreateAdvisesView.as_view(), name='advises'),
    re_path(r'^arbiter/$', CreateArbiterView.as_view(), name='arbiter'),
    re_path(r'^laws/$', RoleListView.as_view(), name='laws'),
    re_path(r'^contact_us/$', CreateContactUsView.as_view(), name='contact_us'),
    re_path(r'^post/(?P<slug>[-\w]+)/$', PostDetailView.as_view(), name='detail'),
    # re_path(r'^criminal/(?P<slug>[-\w]+)/$', CriminalListView.as_view(), name='criminal'),
    re_path(r'^criminal/$', CriminalListView.as_view(), name='criminal'),
    re_path(r'^posts/category/(?P<slug>[-\w]+)/$', ListCategoryPosts.as_view(), name='category'),
    re_path(r'^posts/category/(?P<slug>[-\w]+)/page/(?P<page>[\d]+)/$', ListCategoryPosts.as_view(), name='category'),
]
