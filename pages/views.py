from django.db.models import Q
from django.views.generic import TemplateView, ListView
from django.utils.translation import ugettext_lazy as _
from blog.models import Post, Category
from cms.models import Cms, Quote, Criminal


# Create your views here.
class HomeTemplateView(TemplateView):
    template_name = 'pages/index.html'

    page_name = _('خانه')

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(HomeTemplateView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context


class AboutUsTemplateView(TemplateView):
    template_name = 'pages/profile.html'

    page_name = _('درباره ما')

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(AboutUsTemplateView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context


class SearchResultsView(ListView):
    model = Post
    paginate_by = 20
    template_name = 'pages/search.html'
    context_object_name = 'search_list'

    def get_queryset(self):
        query = self.request.GET.get('param')
        search_list = Post.objects.filter(
            Q(title__icontains=query) | Q(description__icontains=query)
        )
        return search_list.distinct()

    def get_context_data(self, **kwargs):
        cms = Cms.objects.first()
        posts = Post.objects.filter(is_published='P')[:4]
        quote = Quote.objects.order_by('?').first()
        special_post = Post.objects.filter(is_published='P', is_special=True)[:4]
        categories = Category.objects.all()
        criminal = Criminal.objects.all()

        context = super(SearchResultsView, self).get_context_data()

        context['site'] = cms
        context['posts'] = posts
        context['categories'] = categories
        context['quotes'] = quote
        context['special_posts'] = special_post
        context['criminals'] = criminal

        return context